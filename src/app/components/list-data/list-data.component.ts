import { Component, ChangeDetectionStrategy} from '@angular/core';
import { GithubService } from '../../services/github.service';
import { Router,ActivatedRoute } from "@angular/router";

//variables entorno
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-list-data',
  changeDetection: ChangeDetectionStrategy.Default,
  templateUrl: './list-data.component.html',
  styleUrls: ['./list-data.component.scss']
})
export class ListDataComponent {

  //incializar var
  currentRepo:string = ``;
  currentPage:number = 1; //por defecto, primera página
  issues: any [] = [];
  repoData: any [] = [];
  GITHUBENDPOINT: string;

  constructor(public github:GithubService,
              public activatedRouter:ActivatedRoute,
              private router: Router) {
    
    const GITHUBENDPOINT = environment.gitHubApi;

    //recoger parámetros
    this.activatedRouter.params.subscribe( params => {
    
      if( params["user"] ){
        this.currentRepo = GITHUBENDPOINT + params["user"] + "/" + params["repo"];
        this.currentPage=  Number(params["page"]);
        this.setIssues(params);
        this.setRepoData();
      }

    })
  }

  /**
   * setRepoData
   * Almacena en repoData los datos que recibe de la API sobre el repositorio
   * 
  */
  setRepoData(){

    this.github.getRepoInfo(this.currentRepo)
      .subscribe( (data: any) => {
        this.repoData = data;
      });

  }

  /**
    * setIssues
    * Almacena en Issues los datos que recibe de la API sobre las issues de un repositorio
    * 
    * @param params 
  */
  setIssues(params){
  
     this.github.getIssues(this.currentRepo, params["page"])
       .subscribe( (data: any) => {
         this.issues = data;
       });
  }

  /**
  * paginate
  * Gestiona la paginación del listado
  * 
  * @param val 
  */
  paginate(val){
    
    switch (val) {
      case 'next':
        this.currentPage++;
        break;
      
      case 'prev':
        this.currentPage <= 1 ? this.currentPage = 1 : this.currentPage--;
        break;
      
      default:
        break;
    }

    this.github.getIssues(this.currentRepo, this.currentPage)
      .subscribe( (data: any) => {
        this.issues = data;
      });
    
    return false;

  }
}
