import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  repositoryData: any[] = [];
  currentPage : number = 1;
  
  constructor(private router: Router) {}

  /**
   * search
   * Recoge los datos a buscar y gestiona el envío a la página que los mostrará
   * 
   * @param repositoryName String: Nombre del repositorio
   */
  search(repositoryName){

    //Validación
    if(repositoryName == ""){
      alert("Necesito que introduzcas un repositorio");
      return false;
    }
  
    if( !repositoryName.includes("github.com") ) {
      alert("Vaya! Parece que el texto no es un repositorio de github");
      return false;
    }

    let repoDataTemp;
    
    repoDataTemp = repositoryName.split("/");

    //recoger data
    this.repositoryData["user"] = repoDataTemp[3]; //usuario repositorio
    this.repositoryData["repo"] = repoDataTemp[4]; //nombre repositorio
    this.repositoryData["URL"] =  repositoryName; //URL repositorio

    //enviar data a la ruta
    this.router.navigate(['buscar' , this.repositoryData["user"], this.repositoryData["repo"], this.currentPage ]) 

  }
}
