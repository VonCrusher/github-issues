import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class GithubService {
  
  constructor(private http: HttpClient) {}
  
  /**
   * getQuery
   * Procesa las consultas a la API y agrega las cabeceras
   * 
   * @param query String : petición a la API
   */
  getQuery(query: string){

    const headers = new HttpHeaders({
      'Accept' : 'application/vnd.github.symmetra-preview+json'
    });   

    return this.http.get(query, { headers });
  }

  /**
   * getRepoInfo
   * Devuelve información sobre el repositorio consultado
   * 
   * @param repo String: Nombre del repositorio
   */
  getRepoInfo(repo){
    return this.getQuery(repo);
  }

  /**
   * getIssues
   * Devuelve las issues de un repositorio en concreto
   * 
   * @param repo String: Nombre del repositorio
   * @param page Number: Página a buscar
   */
  getIssues(repo, page){    
    let query = repo + `/issues?page=${page}&per_page=10`;
    return this.getQuery(query);
  }

}
