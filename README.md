# GithubIssues

Recoger información e issues de un repositorio cualquiera de GitHub mediante una URL. Para ello, hace uso de la API de GitHub.

## Requisitos

Antes de empezar, deberás tener en tu entorno lo siguiente:

* NodeJS version 8.x or 10.x
* NPM
* Angular-CLI 
* Recomendable: tu editor de código de cabecera y café

Para instalar angular-CLI podemos hacerlo de forma sencilla desde la terminal. Una vez situados en la carpeta del proyecto, ejecutaremos:

`npm install -g @angular/cli`

## Comenzando

Para hacer uso de esta aplicación, deberás clonarte el repositorio mediante GIT a tu entorno local de desarrollo. Es un desarrollo basado en Angular 7, así que, para arrancarlo, será suficiente con que lanzes un `ng serve` desde una terminal que se encuentr en el directorio del proyecto.

Después, simplemente accede a `http://localhost:4200/` para poder verlo en funcionamiento.

## Uso

Una vez cargada la aplicación, sólo deberás introducir la URL en el campo que aparece y te mostrará información útil sobre el repositorio y sobre sus Issues.

## API Github

Esta aplicación consume directamente la API pública de GitHub. Para ver más información sobre la misma, recomiendo visitar su sitio web [Github Api](https://api.github.com) 

### Variables de entorno

La URL de conexión a la API está declarada como variable de entorno, para facilitar futuros cambios y el mantenimiento de la aplicación. Esta variable se encuentra en bajo `src/environments`

## Creado con 🛠️

* [Bootstrap](https://getbootstrap.com/docs/4.1) - Framework CSS
* [Angular](https://angular.io) - Framework Javascript
* [VisualStudioCode](https://code.visualstudio.com/) - ~~IDE~~ Editor de código
* [BitBucket](https://bitbucket.org/#) - Almacén de repositorios

Desarrollado íntegramente bajo GNU/Linux, usando [ElementaryOS](https://elementary.io/es/) 


## Últimos apuntes

También se debería activar CORS para poder evitar problemas al usar la aplicación. Para ello, se recomienda usar una extensión del navegador

* Google Chrome: [Moesif]('https://chrome.google.com/webstore/detail/moesif-origin-cors-change/digfbfaphojjndkpccljibejjbppifbc')
* Mozilla Firefox: [Cors Everywhere]('https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/')